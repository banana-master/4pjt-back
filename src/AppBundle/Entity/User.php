<?php
namespace AppBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity
* @ApiResource(attributes={
*     "normalization_context"={"groups"={"user", "user-read", "step"}},
*     "denormalization_context"={"groups"={"user", "user-write", "step"}},
*     "filters"={"user.search"}
* })
*/
class User extends BaseUser {
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  * @Groups({"user"})
  * @var integer
  */
  protected $id;

  /**
  * @Groups({"user"})
  * @Assert\NotBlank
  * @Assert\Email
  * @var string
  */
  protected $email;

  /**
  * @ORM\Column(type="string", length=255, nullable=true)
  * @Groups({"user"})
  * @Assert\Length(max=255)
  * @var string
  */
  protected $fullname;

  /**
  * @Groups({"user-write"})
  * @Assert\NotBlank
  * @var string
  */
  protected $plainPassword;

  /**
  * @Groups({"user"})
  * @Assert\NotBlank
  * @var string
  */
  protected $username;

  /**
  * @ORM\Column(type="string", length=16, nullable=true)
  * @Groups({"user"})
  * @Assert\NotBlank
  * @Assert\Length(max = 16)
  * @Assert\Regex(pattern = "/^((\+[0-9]+)?\(0\))?[0-9]{9,10}$/")
  * @var string
  */
  protected $phoneNumber;

  /**
  * @Groups({"user"})
  * @Assert\NotBlank
  * @Assert\Choice(callback = "getAvailableRoles")
  * @var string
  */
  protected $role;

  public static function getAvailableRoles() {
    return array(
      "ROLE_ADMIN",
      "ROLE_BUSINESS_ADMIN",
      "ROLE_SHOP_ADMIN",
      "ROLE_CUSTOMER"
    );
  }

  /**
  * Get the value of Id
  *
  * @return mixed
  */
  public function getId() {
    return $this->id;
  }

  /**
  * Set the value of Id
  *
  * @param mixed id
  *
  * @return self
  */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
  * Get the value of Username
  *
  * @return mixed
  */
  public function getUsername() {
    return $this->username;
  }

  /**
  * Set the value of Username
  *
  * @param mixed username
  *
  * @return self
  */
  public function setUsername($username) {
    $this->username = $username;

    return $this;
  }

  public function setFullname($fullname) {
    $this->fullname = $fullname;

    return $this;
  }

  public function getFullname() {
    return $this->fullname;
  }

  /**
  * Set the value of Phone Number
  *
  * @param integer phoneNumber
  *
  * @return self
  */
  public function setPhoneNumber($phoneNumber) {
    $this->phoneNumber = $phoneNumber;

    return $this;
  }

  /**
  * Get the value of Phone Number
  *
  * @return string
  */
  public function getPhoneNumber() {
    return $this->phoneNumber;
  }

  /**
  * Get the value of Role
  *
  * @return mixed
  */
  public function getRole() {
    if (!count($this->roles)) return null;
    return $this->roles[0];
  }

  /**
  * Set the value of Role
  *
  * @param mixed role
  * @return self
  */
  public function setRole($role) {
    $this->roles = array($role);
    $this->role = $role;
    return $this;
  }
}
