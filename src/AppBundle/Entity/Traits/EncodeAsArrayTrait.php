<?php
namespace AppBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
* Encode recursively the object as an array, in order to encode it as JSON easily.
*
* It is a substitute to :
* - json_encode, which can't get protected or private properties
* - Symfony serializer, which is intriguingly long with DateTime fields
*/
trait EncodeAsArrayTrait {
  /**
  * @SuppressWarnings(PHPMD.CyclomaticComplexity)
  * (cannot be further factorized, unless splitting into additional methods)
  */
  public function encodeAsArray($maxDepth = 15, $cache = [], $ident = "id") {
    if ($maxDepth === 0) return "__max-depth-reached__";
    if ($cache !== null) {
      $identifier = get_class($this) . "-" . $this->$ident;
      if (in_array($identifier, $cache)) return "__circular-reference__";
      $cache[] = $identifier;
    }
    $properties = get_object_vars($this);
    foreach ($properties as $name => $value) {
      if (is_object($value) && method_exists($value, "encodeAsArray")) {
        $properties[$name] = $value->encodeAsArray($maxDepth - 1, $cache);
      } elseif ($value instanceof \DateTimeInterface) {
        $properties[$name] = $value->format(\DateTime::RFC822);
      } elseif (is_array($value)) foreach ($value as $key => $currval) {
        if (is_object($currval) && method_exists($currval, "encodeAsArray")) {
          $properties[$name][$key] = $currval->encodeAsArray($maxDepth - 1, $cache);
        }
      }
    }
    return $properties;
  }
}
