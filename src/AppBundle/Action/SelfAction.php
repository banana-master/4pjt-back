<?php
namespace AppBundle\Action;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

use AppBundle\Entity\User;

class SelfAction {
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
    * @Route(
    *     name="self",
    *     path="/self",
    *     defaults={"_api_resource_class"=User::class, "_api_collection_operation_name"="self"}
    * )
    * @Method("GET")
    */
    public function __invoke() {
        return $this->tokenStorage->getToken()->getUser();
    }
}
