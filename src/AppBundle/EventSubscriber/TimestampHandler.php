<?php
namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

/**
* Automatic timestamp setting for entities
*/
class TimestampHandler implements EventSubscriber {
  protected $at = null;
  public function getSubscribedEvents() {
    return array(Events::prePersist, Events::preUpdate);
  }

  protected function getAt() {
    if ($this->at === null) $this->at = new \DateTime('now');
    return $this->at;
  }

  public function prePersist(LifecycleEventArgs $args) {
    $item = $args->getEntity();
    if (method_exists($item, 'setCreatedAt')) {
      $item->setCreatedAt($this->getAt());
    }
  }

  public function preUpdate(LifecycleEventArgs $args) {
    $item = $args->getEntity();
    if (method_exists($item, 'setUpdatedAt')) {
      $item->setUpdatedAt($this->getAt());
    }
  }
}
