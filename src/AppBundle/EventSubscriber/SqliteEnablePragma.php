<?php
namespace AppBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\DBAL\Events;
use Doctrine\DBAL\Event\ConnectionEventArgs;

class SqliteEnablePragma implements EventSubscriber {

  public function getSubscribedEvents() {
    return array(Events::postConnect);
  }

  /**
  * @param \Doctrine\DBAL\Event\ConnectionEventArgs $args
  * @return void
  */
  public function postConnect(ConnectionEventArgs $args) {
    if ($args->getConnection()->getDatabasePlatform()->getName() !== "sqlite") return;
    $args->getConnection()->exec("PRAGMA foreign_keys = ON;");
  }
}
